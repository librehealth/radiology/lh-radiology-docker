# Docker installation on Windows10

Docker is a computer program that performs operating-system-level virtualization, also known as "containerization". Docker allows you to install software quickly and without worrying about dependencies.
To use Docker images of software, you need to have Docker installed on your machine.

To check if you have Docker, run the terminal: click the "Start >> Program Files >> Accessories >> Command Prompt" to open a Command Prompt session using just your mouse. Click the "Start" button and type "cmd." Right-click "Cmd," select "Run as Administrator" and click "Yes" to open Command Prompt with elevated privileges.

Type in the terminal:
```
docker -v
```
If you do not see the version as an output, you need to install Docker. Most modern computes can run Docker, if you have any doubts, please check the [official documentation](https://docs.docker.com/).

### Downloading and installing Docker

Please visit the official page of [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/install/) and click the Download button.

![](images/windows/docker_official_page.png)

You will be redirected to Dockerhub. You need to register and login to download the installation file.

![](images/windows/logging_in.png)

The registration process is very simple and straightforward. After registering, enter you login and password.

![](images/windows/signing_in.jpg)

You will be redirected to Dockerhub. Now the download button will be enabled.

![](images/windows/downloading_docker.png)

Navigate to the directory where you saved the installation file. Double click the installation file.

![](images/windows/windows_installer.png)

![](images/windows/download_progress.jpg)

Accept the defaults.
![](images/windows/configuration.jpg)

The installation will continue.
![](images/windows/docker_installation_1.jpg)

![](images/windows/docker_installation_2.jpg)

Open the terminal and type:
```
docker -v
```
You should see the similar output.

![](images/windows/docker_version.png)

Navigate to the folder where you installed Docker and double click on the *Docker for Windows.exe* file.
![](images/windows/docker_executable.png)

Soon you will see a pop-up window. Enter your credentials.

![](images/windows/logging_in_to_docker.jpg)

## Running the container

Clone the project by typing the following command to the command line:

```
git clone https://gitlab.com/librehealth/radiology/lh-radiology-docker

```
![](images/linux/clonning_repo.png)

Navigate to the directory where you cloned this project. Depending on how you want to interact with the container, run it in foreground or as a daemon.
To run the container in the foreground:
```
docker-compose -f docker-compose.dev.yml up
```
![](images/linux/running_container_foreground.png)

MySQL will be started first and then lh-toolkit will be started on the containers.
When you are done using lh-toolkit you can press `Ctrl+C` to stop the container.

To run the container in the background:
```
docker-compose -f docker-compose.dev.yml up -d
```
![](images/linux/running_container_background.png)


## Using lh-toolkit
To start using lh-toolkit, point your browser to localhost:8080/lh-toolkit .
The following are the authentication information:

* **User**: admin
* **Pass**: Admin123

![](images/linux/login_screen.png)

![](images/linux/welcome_screen.jpg)

## Bringing container down
To bring the container down and to free space on your machine run:
```
docker-compose down
```
![](images/linux/bringing_container_down.png)

## Troubleshooting
When you are pulling the container image, the directory you are in does not matter.
However, if you try to run this docker image from outside of this project directory, you will get the following error:

![](images/linux/running_image_from_wrong_dir.png)

Navigate to the project directory and enter the command again.

[Docker Compose]: https://docs.docker.com/compose/install/
[Docker]: https://docs.docker.com/engine


## Bringing container down
To bring the container down and to free space on your machine run:
```
docker-compose down
```

![](images/linux/bringing_container_down.png)

## Troubleshooting
When you are pulling the container image, the directory you are in does not matter.
However, if you try to run this docker image from outside of this project directory, you will get the following error:

![Screenshot](images/linux/running_image_from_wrong_dir.png)

Navigate to the project directory and enter the command again.

If you try to pull an image before starting the Docker daemon, you will see an error:

![](images/linux/docker_not_started_error.png)

Navigate to the folder where you installed Docker and double click on the *Docker for Windows.exe* file.

You might see the following warning on the older systems:
![](images/windows/windows_deprecated.jpg)

Ignore it or update your system.

### Helpful links

[Docker Compose](https://docs.docker.com/compose/install)

[Docker](https://docs.docker.com/engine)
